
Java Logical Programs 
======================= 

1. Add two numbers without using '+' operator
 


1. Program: Find out duplicate number between 1 to N numbers.
Description:
	You have got a range of numbers between 1 to N, where one of the number is repeated. You need to write a program to find out the duplicate number

	
	
import java.util.ArrayList;
import java.util.List;
 
public class DuplicateNumber {
 
    public int findDuplicateNumber(List<Integer> numbers){
         
        int highestNumber = numbers.size() - 1;
        int total = getSum(numbers);
        int duplicate = total - (highestNumber * (highestNumber + 1) / 2);
        return duplicate;

    }
     
    public int getSum(List<Integer> numbers){
         
        int sum = 0;
        for(int num:numbers){
			sum += num;
        }
        return sum;
    }
     
    public static void main(String a[]){

        List<Integer> numbers = new ArrayList<Integer>();
        for(int i=1;i<30;i++){
			numbers.add(i);
        }

        //add duplicate number into the list
        numbers.add(22);
        DuplicateNumber dn = new DuplicateNumber();
        System.out.println("Duplicate Number: "+dn.findDuplicateNumber(numbers));
    }
}

---------------------------------------------------------------------------------------------------------------------------------------------------------------

2. Program: Find out middle index where sum of both ends are equal.

Description:
	You are given an array of numbers. Find out the array index or position where sum of numbers preceeding the index is equals to sum of numbers succeeding the index.
	
	
	public class FindMiddleIndex {

			public static int findMiddleIndex(int[] numbers) throws Exception {

				int endIndex = numbers.length - 1;
				int startIndex = 0;
				int sumLeft = 0;
				int sumRight = 0;
				while (true) {
					if (sumLeft > sumRight) {
						sumRight += numbers[endIndex--];
					} else {
						sumLeft += numbers[startIndex++];
					}
					if (startIndex > endIndex) {
						if (sumLeft == sumRight) {
							break;
						} else {
							throw new Exception(
									"Please pass proper array to match the requirement");
						}
					}
				}
				return endIndex;
			}

			public static void main(String a[]) {
				int[] num = { 2, 4, 4, 5, 4, 1 };
				try {
					System.out.println("Starting from index 0, adding numbers till index "
									+ findMiddleIndex(num) + " and");
					System.out.println("adding rest of the numbers can be equal");
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
			}
	}

---------------------------------------------------------------------------------------------------------------------------------------------------------------

3. Program: Write a singleton class.

Description:
Singleton class means you can create only one object for the given class. You can create a singleton class by making its constructor as private, so that you can restrict the creation of the object. Provide a static method to get instance of the object, wherein you can handle the object creation inside the class only. In this example we are creating object by using static block.


		public class MySingleton {

			private static MySingleton myObj;
			
			static{
				myObj = new MySingleton();
			}
			
			private MySingleton(){}
			
			public static MySingleton getInstance(){
				return myObj;
			}
			
			public void testMe(){
				System.out.println("Hey.... it is working!!!");
			}
			
			public static void main(String a[]){
				MySingleton ms = getInstance();
				ms.testMe();
			}
	}

---------------------------------------------------------------------------------------------------------------------------------------------------------------


4. Program: Write a program to create deadlock between two threads.

Description:
Deadlock describes a situation where two or more threads are blocked forever, waiting for each other. Deadlocks can occur in Java when the synchronized keyword causes the executing thread to block while waiting to get the lock, associated with the specified object. Since the thread might already hold locks associated with other objects, two threads could each be waiting for the other to release a lock. In such case, they will end up waiting forever.


public class MyDeadlock {

	String str1 = "Java";
	String str2 = "UNIX";
	
	Thread trd1 = new Thread("My Thread 1"){
		public void run(){
			while(true){
				synchronized(str1){
					synchronized(str2){
						System.out.println(str1 + str2);
					}
				}
			}
		}
	};
	
	Thread trd2 = new Thread("My Thread 2"){
		public void run(){
			while(true){
				synchronized(str2){
					synchronized(str1){
						System.out.println(str2 + str1);
					}
				}
			}
		}
	};
	
	public static void main(String a[]){
		MyDeadlock mdl = new MyDeadlock();
		mdl.trd1.start();
		mdl.trd2.start();
	}
}


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------


5. Program: Write a program to reverse a string using recursive algorithm.

Description:
	Write a program to reverse a string using recursive methods. You should not use any string reverse methods to do this.

	public class StringRecursiveReversal {

		String reverse = "";
	
		public String reverseString(String str){
			
			if(str.length() == 1){
				return str;
			} else {
				reverse += str.charAt(str.length()-1)
						+reverseString(str.substring(0,str.length()-1));
				return reverse;
			}
		}
	
		public static void main(String a[]){
			StringRecursiveReversal srr = new StringRecursiveReversal();
			System.out.println("Result: "+srr.reverseString("Java2novice"));
		}
	}

	
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

6. Program: Write a program to reverse a number.

Description:
Write a program to reverse a number using numeric operations. Below example shows how to reverse a number using numeric operations.


public class NumberReverse {

	public int reverseNumber(int number){
		
		int reverse = 0;
		while(number != 0){
			reverse = (reverse*10)+(number%10);
			number = number/10;
		} 
		return reverse;
	}
	
	public static void main(String a[]){
		NumberReverse nr = new NumberReverse();
		System.out.println("Result: "+nr.reverseNumber(17868));
	}
}	


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------


7. Program: Write a program to convert decimal number to binary format.

Description:
Write a program to convert decimal number to binary format using numeric operations. Below example shows how to convert decimal number to binary format using numeric operations.
1 

public class DecimalToBinary {

	public void printBinaryFormat(int number){
		int binary[] = new int[25];
		int index = 0;
		while(number > 0){
			binary[index++] = number % 2;
			number = number / 2;
		}
		for(int i = index-1;i >= 0;i--){
			System.out.print(binary[i]);
		}
	}
	
	public static void main(String a[]){
		DecimalToBinary dtb = new DecimalToBinary();
		dtb.printBinaryFormat(25);
	}
}

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------



8. Program: Write a program to find perfect number or not.

Description:
	A perfect number is a positive integer that is equal to the sum of its proper positive divisors, that is, the sum of its positive divisors excluding the number itself. Equivalently,a perfect number is a number that is half the sum of all of its positive divisors. The first perfect number is 6, because 1, 2 and 3 are its proper positive divisors, and 1 + 2 + 3 = 6. Equivalently, the number 6 is equal to half the sum of all its positive divisors:
		( 1 + 2 + 3 + 6 ) / 2 = 6.
		
		
	public class IsPerfectNumber {

	public boolean isPerfectNumber(int number){
		
		int temp = 0;
		for(int i=1;i<=number/2;i++){
			if(number%i == 0){
				temp += i;
			}
		}
		if(temp == number){
			System.out.println("It is a perfect number");
			return true;
		} else {
			System.out.println("It is not a perfect number");
			return false;
		}
	}
	
	public static void main(String a[]){
		IsPerfectNumber ipn = new IsPerfectNumber();
		System.out.println("Is perfect number: "+ipn.isPerfectNumber(28));
	}
}

Output:
28
It is a perfect number
Is perfect number: true

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------


9. Program: Write a program to implement ArrayList.

Description:
	Write a program to implement your own ArrayList class. It should contain add(), get(), remove(), size() methods. Use dynamic array logic. It should increase its size when it reaches threshold.
	
	import java.util.Arrays;

public class MyArrayList {

	private Object[] myStore;
	private int actSize = 0;
	
	public MyArrayList(){
		myStore = new Object[10];
	}
	
	public Object get(int index){
		if(index < actSize){
			return myStore[index];
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}
	
	public void add(Object obj){
		if(myStore.length-actSize <= 5){
			increaseListSize();
		}
		myStore[actSize++] = obj;
	}
	
	public Object remove(int index){
		if(index < actSize){
			Object obj = myStore[index];
			myStore[index] = null;
			int tmp = index;
			while(tmp < actSize){
				myStore[tmp] = myStore[tmp+1];
				myStore[tmp+1] = null;
				tmp++;
			}
			actSize--;
			return obj;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
		
	}
	
	public int size(){
		return actSize;
	}
	
	private void increaseListSize(){
		myStore = Arrays.copyOf(myStore, myStore.length*2);
		System.out.println("\nNew length: "+myStore.length);
	}
	
	public static void main(String a[]){
		MyArrayList mal = new MyArrayList();
		mal.add(new Integer(2));
		mal.add(new Integer(5));
		mal.add(new Integer(1));
		mal.add(new Integer(23));
		mal.add(new Integer(14));
		for(int i=0;i<mal.size();i++){
			System.out.print(mal.get(i)+" ");
		}
		mal.add(new Integer(29));
		System.out.println("Element at Index 5:"+mal.get(5));
		System.out.println("List size: "+mal.size());
		System.out.println("Removing element at index 2: "+mal.remove(2));
		for(int i=0;i<mal.size();i++){
			System.out.print(mal.get(i)+" ");
		}
	}
}

Output:
2 5 1 23 14 
New length: 20
Element at Index 5:29
List size: 6
Removing element at index 2: 1
2 5 23 14 29 


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

10. Program: Write a program to find maximum repeated words from a file.

Description:
	Write a program to read words from a file. Count the repeated or duplicated words. Sort it by maximum repeated or duplicated word count.
	
	
	
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;

public class MaxDuplicateWordCount {
	
	public Map<String, Integer> getWordCount(String fileName){

		FileInputStream fis = null;
		DataInputStream dis = null;
		BufferedReader br = null;
		Map<String, Integer> wordMap = new HashMap<String, Integer>();
		try {
			fis = new FileInputStream(fileName);
			dis = new DataInputStream(fis);
			br = new BufferedReader(new InputStreamReader(dis));
			String line = null;
			while((line = br.readLine()) != null){
				StringTokenizer st = new StringTokenizer(line, " ");
				while(st.hasMoreTokens()){
					String tmp = st.nextToken().toLowerCase();
					if(wordMap.containsKey(tmp)){
						wordMap.put(tmp, wordMap.get(tmp)+1);
					} else {
						wordMap.put(tmp, 1);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{if(br != null) br.close();}catch(Exception ex){}
		}
		return wordMap;
	}
	
	public List<Entry<String, Integer>> sortByValue(Map<String, Integer> wordMap){
		
		Set<Entry<String, Integer>> set = wordMap.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        return list;
	}
	
	public static void main(String a[]){
		MaxDuplicateWordCount mdc = new MaxDuplicateWordCount();
		Map<String, Integer> wordMap = mdc.getWordCount("C:/MyTestFile.txt");
		List<Entry<String, Integer>> list = mdc.sortByValue(wordMap);
		for(Map.Entry<String, Integer> entry:list){
        	System.out.println(entry.getKey()+" ==== "+entry.getValue());
        }
	}
}

Output:
one ==== 3
the ==== 3
that ==== 3
of ==== 2
in ==== 2
some ==== 2
to ==== 1
summary ==== 1
but ==== 1
have ==== 1
common ==== 1
least ==== 1
simplest ==== 1


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

11. Program: Write a program to find out duplicate characters in a string.

Description:
	Write a program to find out duplicate or repeated characters in a string, and calculate the count of repeatation.
	
	
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateCharsInString {

	public void findDuplicateChars(String str){
		
		Map<Character, Integer> dupMap = new HashMap<Character, Integer>(); 
		char[] chrs = str.toCharArray();
		for(Character ch:chrs){
			if(dupMap.containsKey(ch)){
				dupMap.put(ch, dupMap.get(ch)+1);
			} else {
				dupMap.put(ch, 1);
			}
		}
		Set<Character> keys = dupMap.keySet();
		for(Character ch:keys){
			if(dupMap.get(ch) > 1){
				System.out.println(ch+"--->"+dupMap.get(ch));
			}
		}
	}
	
	public static void main(String a[]){
		DuplicateCharsInString dcs = new DuplicateCharsInString();
		dcs.findDuplicateChars("Java2Novice");
	}
}

Output:
v--->2
a--->2

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------


12. Program: Write a program to find top two maximum numbers in a array.

Description:
	Write a program to find top two maximum numbers in the given array. You should not use any sorting functions. You should iterate the array only once. You should not use any
kind of collections in java.


public class TwoMaxNumbers {

	public void printTwoMaxNumbers(int[] nums){
		int maxOne = 0;
		int maxTwo = 0;
		for(int n:nums){
			if(maxOne < n){
				maxTwo = maxOne;
				maxOne =n;
			} else if(maxTwo < n){
				maxTwo = n;
			}
		}
		System.out.println("First Max Number: "+maxOne);
		System.out.println("Second Max Number: "+maxTwo);
	}
	
	public static void main(String a[]){
		int num[] = {5,34,78,2,45,1,99,23};
		TwoMaxNumbers tmn = new TwoMaxNumbers();
		tmn.printTwoMaxNumbers(num);
	}
}

Output:
First Max Number: 99
Second Max Number: 78
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------


13. Program: Write a program to sort a map by value.

Description:
	Sort or order a HashMap or TreeSet or any map item by value. Write a comparator which compares by value, not by key. Entry class might hleps you here.
	
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class OrderByValue {

	public static void main(String a[]){
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("java", 20);
		map.put("C++", 45);
		map.put("Java2Novice", 2);
		map.put("Unix", 67);
		map.put("MAC", 26);
		map.put("Why this kolavari", 93);
		Set<Entry<String, Integer>> set = map.entrySet();
        List<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare( Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2 )
            {
                return (o2.getValue()).compareTo( o1.getValue() );
            }
        } );
        for(Map.Entry<String, Integer> entry:list){
        	System.out.println(entry.getKey()+" ==== "+entry.getValue());
        }
	}
}

Output:
Why this kolavari ==== 93
Unix ==== 67
C++ ==== 45
MAC ==== 26
java ==== 20
Java2Novice ==== 2
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

14. Program: Write a program to find common elements between two arrays.

Description:
	Write a program to identify common elements or numbers between two given arrays. You should not use any inbuilt methods are list to find common values.
	
	
	public class CommonElementsInArray {

	public static void main(String a[]){
		int[] arr1 = {4,7,3,9,2};
		int[] arr2 = {3,2,12,9,40,32,4};
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					System.out.println(arr1[i]);
				}
			}
		}
	}
}

Output:
4
3
9
2

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

15. Program: How to swap two numbers without using temporary variable?

Description:
	Write a program to swap or exchange two numbers. You should not use any temporary or third variable to swap.

	public class MySwapingTwoNumbers {

	public static void main(String a[]){
		int x = 10;
		int y = 20;
		System.out.println("Before swap:");
		System.out.println("x value: "+x);
		System.out.println("y value: "+y);
		x = x+y;
		y=x-y;
		x=x-y;
		System.out.println("After swap:");
		System.out.println("x value: "+x);
		System.out.println("y value: "+y);
	}
}

Output:
Before swap:
x value: 10
y value: 20
After swap:
x value: 20
y value: 10

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

16. Program: Write a program to print fibonacci series.

public class Fibonacci {

	static int first = 0, second = 1 , result = 0,count = 0;
	
	public static void main(String[] args) {
		
		int num = 10;
		
		System.out.println("Without Recurssion");
		withoutRecursion(num);
		
		
		
		System.out.println("\n\nWith Recurssion");
		System.out.println("0\n1");
		withRecursion(0);
		
		
	}

	private static void withRecursion(int num) {
		
		if(num < 10){
			
			result = first + second;
			
			System.out.println(result+" ");
			
			first = second;
			second = result;
			
			withRecursion((num + 1));
			
		}
		
		
	}

	private static void withoutRecursion(int num) {
	
		int first = 0, second = 1 , result = 0,count = 0;
		
		System.out.println(first+"\n"+second);
		
		while(count < num){
			
			result = first + second;
			
			System.out.println(result+" ");
			
			first = second;
			second = result;
			
			count++;
			
		}
		
		
		
	}

}




// Another one

public class MyFibonacci {

	public static void main(String a[]){
		
		 int febCount = 15;
         int[] feb = new int[febCount];
         feb[0] = 0;
         feb[1] = 1;
         for(int i=2; i < febCount; i++){
        	 feb[i] = feb[i-1] + feb[i-2];
         }

         for(int i=0; i< febCount; i++){
                 System.out.print(feb[i] + " ");
         }
	}
}

Output:
0 1 1 2 3 5 8 13 21 34 55 89 144 233 377 

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

17.  Factorial


public class Factorial {

	static int temp = 1;
	
	public static void main(String[] args) {
		
		int number = 10;
		
		
		long start1 = System.currentTimeMillis();
		factorialWithoutRecursion(number);
		long end1 = System.currentTimeMillis();
		
		System.out.println("Time Taken : "+(end1 - start1));
		
		
		long start2 = System.currentTimeMillis();
		factorialWithRecursion(number);
		long end2 = System.currentTimeMillis();
		
		System.out.println("Time Taken : "+(end2 - start2));
		
	}
	

	private static void factorialWithRecursion(int number) {
		
		if(number > 0){
			temp = temp * number;
			factorialWithRecursion(number-1);
		}
		else{
			
			System.out.println("Number Factorial Using Recursion = "+temp);
		}
		
	}


	private static void factorialWithoutRecursion(int number) {
	
		int num = 1;
		if(number > 0){
			
			try{
				Thread.sleep(300);
			}
			catch(InterruptedException exp){}
			
			
			for(int i = number;i > 0;i--){
			
				num = num * i;
			}
		}
		
		System.out.println("Number Factorial Without Recursion = "+num);
		
		
	}

}

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------


18. Palindrome

public class Palindrome {

	public static void main(String[] args) {
		
		String str = "Malayalam";
		System.out.println("Is "+str+" palindrome = "+checkStringIsPalindromeOrNot(str));
		
		
		int num = 4341;
		System.out.println("Is "+num+" palindrome = "+checkNumberIsPalindromeOrNot(num));
		
		
	}

	private static boolean checkNumberIsPalindromeOrNot(int num) {
		
		int rev = 0,i = 0,temp = num;
		
		while(num > 0){
			i = num % 10;
			rev = (rev * 10) + i;
			num = num / 10;
		}
		
		if(temp == rev){
			return true;
		}
		
		return false;
	}
	
	

	private static boolean checkStringIsPalindromeOrNot(String str) {
	
		String temp = "";
		for(int i=str.length()-1;i>=0;i--){
			temp = temp + str.charAt(i);
		}
		
		if(temp.equalsIgnoreCase(str)){
			return true;
		}
		
		
		
		return false;
		
	}

}

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

19. Prime or Not

public class Prime {

	static int count = 0;
	
	public static void main(String[] args) {
		
		int num = 20;
		
		System.out.println("Prime Numbers Between 0 and "+num+" Without Recusion Method");
		primeWithoutRecursion(num);
		
		System.out.println("\n\nPrime Numbers Between 0 and "+num+" With Recusion Method");
		primeWithRecursion(1,num);
		
		System.out.println("\n\n");
		checkNumberIsPrineOrNot(17);
		
	}

	private static void checkNumberIsPrineOrNot(int num) {
		
//		if(num%2 != 0){
//			System.out.println(num+" Is Prime Number");
//		}
//		else{
//			System.out.println(num+" Is Not Prime Number");
//		}
		
		
	
			
			int count = 0;
			
			for(int i = 2;i < num;i++){
				
				if(num%i == 0){
					count++;
					break;
				}
				
			}
			
			
			if(count == 0){
				System.out.println("The Given Number "+num+" is Prime Number");
			}
			else
			{
				System.out.println("The Given Number "+num+" is not Prime Number");
			}
			
		
	}

	private static void primeWithRecursion(int start,int num) {
		
		if(start <= num){
			
			int count = 0;
			for(int j = 2;j <= (start/2);j++){
				
				if(start%j == 0){
					
					count++;
					break;
					
				}
				
			}
			
			if(count == 0){
				System.out.println(start);
			}
			
			primeWithRecursion((start + 1),num);
			
		}
	}

	private static void primeWithoutRecursion(int num) {
		
		int count = 0;
		
		for(int i = 1;i <= num;i++){
			
			count = 0;
			
			for(int j = 2;j <= (i/2) ; j++){
				
				if(i%j == 0){
					count++;
					break;
				}
				
			}
			
			if(count == 0){
				System.out.println(i);
			}
			
		}
		
	}

}

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

20. Reverse

public class Reverse {

	public static void main(String[] args) {
		
		String str = "Suresh";
		int num = 4561212;
		
		System.out.println(num+" Reverse Of "+reverseTheNumber(num));
		System.out.println(str+" Reverse Of "+reverseTheString(str));
		
	}

	private static String reverseTheString(String str) {
		
		String temp = ""; 
		for(int i = str.length()-1;i >= 0;i--){
			temp = temp + str.charAt(i);
		}
		
		return temp;
	}

	private static int reverseTheNumber(int num) {
		
		int rev = 0;
		int i=0;
		while(num > 0){
			
			i = num % 10;
			rev = (rev * 10) + i;
			num = num / 10;
			
		}
		
		
		return rev;
		
	}

}

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

21. Binary Search


import java.util.Scanner;

public class BinarySearch {

	public static void main(String[] args) {
		
		int arr[] = {10,20,30,40,50,60,70,80,90,100};
		int searchElement = 30;
		
		
		
		print(arr);
		System.out.println("Enter Number To Search ");
		
		Scanner scanr = new Scanner(System.in);
		searchElement = scanr.nextInt();
		
		System.out.println("\n\n");
		
		
		int pos = bSearch(arr,searchElement);
		
		if(pos == -1){
			System.out.println(searchElement+" not found...");
		}
		
		if(pos > -1){
			System.out.println(searchElement+" found at Position "+pos);
		}
		
	}

	
	
	
	private static void print(int arr[]) {
	
		int count = 0;
		for(int ar : arr){
			System.out.println("indx: "+count+" - "+ar);
			count++;
		}		
		
	}




	private static int bSearch(int[] arr,int search) {
		
		int first = 0 , last = arr.length-1 , middle = (first + last) / 2;
		
		while(first <= last){
			
			if(arr[middle] < search){
				first = middle + 1;
			}
			
			if(arr[middle] == search){
				return middle;
			}
			
			if(arr[middle] > search){
				last = middle - 1;
				first = 0;
			}
			
			middle = (first + last) / 2;
			
		}
		
		return -1;
	}



}
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

22. Linear Search

public class Linear {

	static int numArr[] = {32,11,31,65,22,76,1,5,8,21};
	
	public static void main(String[] args) {
		
		int search = 1;
		System.out.println(search+" Found At Location "+ SequentialSearch(search));
		
	}

	private static int SequentialSearch(int num) {
	
		for(int i=0;i<numArr.length;i++){
			if(num == numArr[i]){
				return i;
			}
		}
		
		return -1;
	}
	
	
}

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

23. Bubble Sort

public class BubbleSort {

	public static void main(String[] args) {
		
		int arr[] = {130,12,43,11,34,221,41,87,132,441,14};
		
		
		System.out.println("Array Before Sorting");
		print(arr);
		
		bubbleSort(arr);
		
		System.out.println("Array After Sorting");
		print(arr);
		
	}

	
	
	private static void bubbleSort(int[] arr) {
		
		for(int i = 0;i < arr.length;i++){
			
			for(int j = 0;j < arr.length;j++){
				
				if(j != arr.length-1){
					
					if(arr[j+1] < arr[j]){
						
							int temp =0;
							temp = arr[j+1];
							arr[j+1] = arr[j];
							arr[j] = temp;
							
					}
					
				}
				
			}
			
		}
			
	}

	private static void print(int[] arr) {
		for(int ar:arr){
			System.out.println(ar);
		}
	}
	
}


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


23. Selection Sort

package sorting;

public class SelectionSort {

	public static void main(String[] args) {
		
		int arr[] = {130,12,43,11,34,221,41,87,132,441,14};
		
		System.out.println("Array Before Sorting");
		print(arr);
		
		selectionSort(arr);
		
		System.out.println("Array After Sorting");
		print(arr);
		
	}
	
	
	
	
	private static void selectionSort(int[] arr) {
		 
		int first = 0;
		int last = arr.length - 1;
		while(first < last){
			
			int indx = findLowestPositionValue(arr,first,arr.length-1);

			int temp = arr[first];
			arr[first] = arr[indx];
			arr[indx] = temp;
			
			
			first++;
		
		}
		
	}

	 //{130,12,43,11,34,221,41,87,132,441,14};
	
	private static int findLowestPositionValue(int[] arr, int first, int last) {
		
		int temp = arr[first];
		
		int pos = first;
		
		for(int i = first;i < last;i++){
			
			if(temp > arr[i + 1]){
				
				temp = arr[i + 1];
				pos = (i + 1);
				
			}
		}
	
		return pos;
	}


	private static void print(int[] arr) {
		for(int ar:arr){
			System.out.println(ar);
		}
	}
	
}


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

24. Call inner class Parent


class Outer{
	
	public Outer(){
		System.out.println("PARENT CONSTRUCTOR");
	}
	
	
	class InnerOne{
		
		public InnerOne(){
			System.out.println("parent1 constructor");
		}
		
		
		class Inner{
			
			public Inner(String str){
				System.out.println("Parent2 constructor "+str);
			}
		}
		
	}
	
}

//
//class MyClass extends Outer.InnerOne{
//	
//	public MyClass(){
//		
//		new Outer().super();
//		
//	}
//	
//	
//	
//	
//}
//
//
//class Child extends Outer.InnerOne {
//	
//	public Child(){
//		new Outer().super();
//		System.out.println("Child Constructor");
//	}
//	
//}


class MyChild extends Outer.InnerOne.Inner{
	
	public MyChild(){
		new Outer().new InnerOne().super("Sample");
	}
}


public class SuperDemo {
	
	public static void main(String[] args) {		
		new MyChild();
		
		Outer.InnerOne.Inner innrOne = new Outer().new InnerOne().new Inner("dfdf"); 
		
	}
	
}

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

25. Program: Write a program to find sum of each digit in the given number using recursion.

Description:
Below example shows how to find out sum of each digit in the given number using recursion logic. For example, if the number is 259, then the sum should be 2+5+9 = 16.


Code:
package com.java2novice.algos;

public class MyNumberSumRec {

	int sum = 0;
	
	public int getNumberSum(int number){
		
		if(number == 0){
			return sum;
		} else {
			sum += (number%10);
			getNumberSum(number/10);
		}
		return sum;
	}
	
	public static void main(String a[]){
		MyNumberSumRec mns = new MyNumberSumRec();
		System.out.println("Sum is: "+mns.getNumberSum(223));
	}
}

Output:
Sum is: 7


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

26. Program: Write a program to find the given number is Armstrong number or not?

Description:
Armstrong numbers are the sum of their own digits to the power of
the number of digits. It is also known as narcissistic numbers.

Code:
package com.java2novice.algos;

public class MyArmstrongNumber {

	public boolean isArmstrongNumber(int number){
		
		int tmp = number;
		int noOfDigits = String.valueOf(number).length();
		int sum = 0;
		int div = 0; 
		while(tmp > 0) 
		{ 
			div = tmp % 10; 
			int temp = 1;
			for(int i=0;i<noOfDigits;i++){
				temp *= div;
			}
			sum += temp;
			tmp = tmp/10; 
		} 
		if(number == sum) {
			return true; 
		} else {
			return false; 
		} 
	}
	
	public static void main(String a[]){
		MyArmstrongNumber man = new MyArmstrongNumber();
		System.out.println("Is 371 Armstrong number? "+man.isArmstrongNumber(371));
		System.out.println("Is 523 Armstrong number? "+man.isArmstrongNumber(523));
		System.out.println("Is 153 Armstrong number? "+man.isArmstrongNumber(153));
	}
}

Output:
Is 371 Armstrong number? true
Is 523 Armstrong number? false
Is 153 Armstrong number? true

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

27. Program: Write a program to convert binary to decimal number.

Description:
Write a program to convert binary format to decimal number using numeric operations. Below example shows how to convert binary to decimal format using numeric operations.


Code:
package com.java2novice.algos;

public class BinaryToDecimal {

	public int getDecimalFromBinary(int binary){
		
		int decimal = 0;
		int power = 0;
		while(true){
			if(binary == 0){
				break;
			} else {
				int tmp = binary%10;
				decimal += tmp*Math.pow(2, power);
				binary = binary/10;
				power++;
			}
		}
		return decimal;
	}
	
	public static void main(String a[]){
		BinaryToDecimal bd = new BinaryToDecimal();
		System.out.println("11 ===> "+bd.getDecimalFromBinary(11));
		System.out.println("110 ===> "+bd.getDecimalFromBinary(110));
		System.out.println("100110 ===> "+bd.getDecimalFromBinary(100110));
	}
}

Output:
11 ===> 3
110 ===> 6
100110 ===> 38

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

28. Program: Write a program to check the given number is binary number or not?

Description:
The binary numeral system, or base-2 number system, represents numeric values using two symbols: 0 and 1. More specifically, the usual base-2 system is a positional notation with a radix of 2. Because of its straightforward implementation in digital electronic circuitry using logic gates, the binary system is used internally by almost all modern computers.


Code:
package com.java2novice.algos;

public class MyBinaryCheck {

	public boolean isBinaryNumber(int binary){
		
		boolean status = true;
		while(true){
			if(binary == 0){
				break;
			} else {
				int tmp = binary%10;
				if(tmp > 1){
					status = false;
					break;
				}
				binary = binary/10;
			}
		}
		return status;
	}
	
	public static void main(String a[]){
		MyBinaryCheck mbc = new MyBinaryCheck();
		System.out.println("Is 1000111 binary? :"+mbc.isBinaryNumber(1000111));
		System.out.println("Is 10300111 binary? :"+mbc.isBinaryNumber(10300111));
	}
}

Output:
Is 1000111 binary? :true
Is 10300111 binary? :false

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

29. Program: Write a program to implement hashcode and equals.

Description:
The hashcode of a Java Object is simply a number, it is 32-bit signed int, that allows an object to be managed by a hash-based data structure. We know that hash code is an unique id number allocated to an object by JVM. But actually speaking, Hash code is not an unique number for an object. If two objects are equals then these two objects should return same hash code. So we have to implement hashcode() method of a class in such way that if two objects are equals, ie compared by equal() method of that class, then those two objects must return same hash code. If you are overriding hashCode you need to override equals method also. 

The below example shows how to override equals and hashcode methods. The class Price overrides equals and hashcode. If you notice the hashcode implementation, it always generates unique hashcode for each object based on their state, ie if the object state is same, then you will get same hashcode. A HashMap is used in the example to store Price objects as keys. It shows though we generate different objects, but if state is same, still we can use this as key.


Code:
package com.java2novice.algos;

import java.util.HashMap;

public class MyHashcodeImpl {

	public static void main(String a[]){
		
		HashMap<Price, String> hm = new HashMap<Price, String>();
		hm.put(new Price("Banana", 20), "Banana");
		hm.put(new Price("Apple", 40), "Apple");
		hm.put(new Price("Orange", 30), "Orange");
		//creating new object to use as key to get value
		Price key = new Price("Banana", 20);
		System.out.println("Hashcode of the key: "+key.hashCode());
		System.out.println("Value from map: "+hm.get(key));
	}
}

class Price{
	
	private String item;
	private int price;
	
	public Price(String itm, int pr){
		this.item = itm;
		this.price = pr;
	}
	
	public int hashCode(){
		System.out.println("In hashcode");
		int hashcode = 0;
		hashcode = price*20;
		hashcode += item.hashCode();
		return hashcode;
	}
	
	public boolean equals(Object obj){
		System.out.println("In equals");
		if (obj instanceof Price) {
			Price pp = (Price) obj;
			return (pp.item.equals(this.item) && pp.price == this.price);
		} else {
			return false;
		}
	}
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	public String toString(){
		return "item: "+item+"  price: "+price;
	}
}

Output:
In hashcode
In hashcode
In hashcode
In hashcode
Hashcode of the key: 1982479637
In hashcode
In equals
Value from map: Banana

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


30. Program: How to get distinct elements from an array by avoiding duplicate elements?

Description:
The below example shows how to avoid duplicate elements from an array and disply only distinct elements. Please use only arrays to process it.


Code:
package com.java2novice.algos;

public class MyDisticntElements {

	public static void printDistinctElements(int[] arr){
		
		for(int i=0;i<arr.length;i++){
			boolean isDistinct = false;
			for(int j=0;j<i;j++){
				if(arr[i] == arr[j]){
					isDistinct = true;
					break;
				}
			}
			if(!isDistinct){
				System.out.print(arr[i]+" ");
			}
		}
	}
	
	public static void main(String a[]){
		
		int[] nums = {5,2,7,2,4,7,8,2,3};
		MyDisticntElements.printDistinctElements(nums);
	}
}

Output:
5 2 7 4 8 3 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

31. 
Program: Write a program to get distinct word list from the given file.

Description:
Write a program to find all distinct words from the given file. Remove special chars like ".,;:" etc. Ignore case sensitivity.


Code:
package com.java2novice.algos;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MyDistinctFileWords {

	public List<String> getDistinctWordList(String fileName){

		FileInputStream fis = null;
		DataInputStream dis = null;
		BufferedReader br = null;
		List<String> wordList = new ArrayList<String>();
		try {
			fis = new FileInputStream(fileName);
			dis = new DataInputStream(fis);
			br = new BufferedReader(new InputStreamReader(dis));
			String line = null;
			while((line = br.readLine()) != null){
				StringTokenizer st = new StringTokenizer(line, " ,.;:\"");
				while(st.hasMoreTokens()){
					String tmp = st.nextToken().toLowerCase();
					if(!wordList.contains(tmp)){
						wordList.add(tmp);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{if(br != null) br.close();}catch(Exception ex){}
		}
		return wordList;
	}
	
	public static void main(String a[]){
		
		MyDistinctFileWords distFw = new MyDistinctFileWords();
		List<String> wordList = distFw.getDistinctWordList("C:/sample.txt");
		for(String str:wordList){
			System.out.println(str);
		}
	}
}

Output:
the
while
statement
verifies
condition
before
entering
into
loop
to
see
whether
next
iteration
should
occur
or
not
do-while
executes
first
without
checking
it
after
finishing
each
will
always
execute
body
of
a
at
least
once

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

32.Program: Write a program to get a line with max word count from the given file.

Description:
Below example shows how to find out the line with maximum number of word count in the given file. In case if it has multiple lines with max number of words, then it has to list all those lines.


Code:
package com.java2novice.algos;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MaxWordCountInLine {

	private int currentMaxCount = 0;
	private List<String> lines = new ArrayList<String>();
	
	public void readMaxLineCount(String fileName){

		FileInputStream fis = null;
		DataInputStream dis = null;
		BufferedReader br = null;
		
		try {
			fis = new FileInputStream(fileName);
			dis = new DataInputStream(fis);
			br = new BufferedReader(new InputStreamReader(dis));
			String line = null;
			while((line = br.readLine()) != null){
				
				int count = (line.split("\\s+")).length;
				if(count > currentMaxCount){
					lines.clear();
					lines.add(line);
					currentMaxCount = count;
				} else if(count == currentMaxCount){
					lines.add(line);
				} 
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try{
				if(br != null) br.close();
			}catch(Exception ex){}
		}
	}

	public int getCurrentMaxCount() {
		return currentMaxCount;
	}

	public void setCurrentMaxCount(int currentMaxCount) {
		this.currentMaxCount = currentMaxCount;
	}

	public List<String> getLines() {
		return lines;
	}

	public void setLines(List<String> lines) {
		this.lines = lines;
	}
	
	public static void main(String a[]){
		
		MaxWordCountInLine mdc = new MaxWordCountInLine();
		mdc.readMaxLineCount("/Users/ngootooru/MyTestFile.txt");
		System.out.println("Max number of words in a line is: "+mdc.getCurrentMaxCount());
		System.out.println("Line with max word count:");
		List<String> lines = mdc.getLines();
		for(String l:lines){
			System.out.println(l);
		}
	}
}

MyTestFile.txt:
true, false, and null might seem like keywords, but they are actually literals.
You cannot use them as identifiers in your programs. The servlet context
is an interface which helps to communicate with other servlets. It contains
information about the Web application and container. It is kind of
application environment. Using the context, a servlet can obtain URL
references to resources, and store attributes that other servlets in the
context can use.

Output:
Max number of words in a line is: 13
Line with max word count:
true, false, and null might seem like keywords, but they are actually literals. 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

33. Program: Write a program to convert string to number without using Integer.parseInt() method.

Description:
Below example shows how to convert string format of a number to number without calling Integer.parseInt() method. We can do this by converting each character into ascii format and form the number.


Code:
package com.java2novice.algos;

public class MyStringToNumber {

	public static int convert_String_To_Number(String numStr){
		
		char ch[] = numStr.toCharArray();
		int sum = 0;
		//get ascii value for zero
		int zeroAscii = (int)'0';
		for(char c:ch){
			int tmpAscii = (int)c;
			sum = (sum*10)+(tmpAscii-zeroAscii);
		}
		return sum;
	}
	
	public static void main(String a[]){
		
		System.out.println("\"3256\" == "+convert_String_To_Number("3256"));
		System.out.println("\"76289\" == "+convert_String_To_Number("76289"));
		System.out.println("\"90087\" == "+convert_String_To_Number("90087"));
	}
}

Output:
"3256" == 3256
"76289" == 76289
"90087" == 90087

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


34. 
Program: Write a program to find two lines with max characters in descending order.

Description:
Write a program to read a multiple line text file and write the 'N' longest lines to the output console, where the file to be read is specified as command line aruguments. The program should read an input file. The first line should contain the value of the number 'N' followed by multiple lines. 'N' should be a valid positive integer.


Code:
package com.longest.lines;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		
		BufferedReader br = null;
		String filePath = args[0];
		int topList = 0;
		Set<Entries> liSet = new TreeSet<Entries>(new MyComp());
		try {
			br = new BufferedReader(new FileReader(new File(filePath)));
			String line = br.readLine();
			topList = Integer.parseInt(line.trim());
			while((line = br.readLine()) != null){
				line = line.trim();
				if(!"".equals(line)){
					liSet.add(new Entries(line.length(), line));
				}
			}
			int count = 0;
			for(Entries ent:liSet){
				System.out.println(ent.line);
				if(++count == topList){
					break;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static class Entries{
		Integer length;
		String line;
		public Entries(Integer l,String line){
			length = l;
			this.line = line;
		}
	}
	
	public static class MyComp implements Comparator<Entries>{

		@Override
		public int compare(Entries e1, Entries e2) {
			if(e2.length > e1.length){
				return 1;
			} else {
				return -1;
			}
		}
		
	}
}

Sample input file:
3
Java2novice
My Test line 123

Java world
I know java language

This is a test program
java is simple

Output:
This is a test program
I know java language
My Test line 123


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

35. Program: Write a program to find the sum of the first 1000 prime numbers.

Description:
Write a program to find the sum of the first 1000 prime numbers.


Code:
package com.primesum;

public class Main {

	public static void main(String args[]){
		
		int number = 2;
		int count = 0;
		long sum = 0;
		while(count < 1000){
			if(isPrimeNumber(number)){
				sum += number;
				count++;
			}
			number++;
		}
		System.out.println(sum);
	}
	
	private static boolean isPrimeNumber(int number){
		
		for(int i=2; i<=number/2; i++){
            if(number % i == 0){
                return false;
            }
        }
        return true;
	}
}

Output:
3682913

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

36. Program: Find longest substring without repeating characters.

Description:
Given a string, find the longest substrings without repeating characters. Iterate through the given string, find the longest maximum substrings.


Code:
package com.java2novice.algos;

import java.util.HashSet;
import java.util.Set;

public class MyLongestSubstr {

	private Set<String> subStrList = new HashSet<String>();
	private int finalSubStrSize = 0;
	
	public Set<String> getLongestSubstr(String input){
		//reset instance variables
		subStrList.clear();
		finalSubStrSize = 0;
		// have a boolean flag on each character ascii value
		boolean[] flag = new boolean[256];
		int j = 0;
		char[] inputCharArr = input.toCharArray();
		for (int i = 0; i < inputCharArr.length; i++) {
			char c = inputCharArr[i];
			if (flag[c]) {
				extractSubString(inputCharArr,j,i);
				for (int k = j; k < i; k++) {
					if (inputCharArr[k] == c) {
						j = k + 1;
						break;
					}
					flag[inputCharArr[k]] = false;
				}	
			} else {
				flag[c] = true;
			}
		}
		extractSubString(inputCharArr,j,inputCharArr.length);
		return subStrList;
	}
	
	private String extractSubString(char[] inputArr, int start, int end){
		
		StringBuilder sb = new StringBuilder();
		for(int i=start;i<end;i++){
			sb.append(inputArr[i]);
		}
		String subStr = sb.toString();
		if(subStr.length() > finalSubStrSize){
			finalSubStrSize = subStr.length();
			subStrList.clear();
			subStrList.add(subStr);
		} else if(subStr.length() == finalSubStrSize){
			subStrList.add(subStr);
		}
		
		return sb.toString();
	}

	public static void main(String a[]){
		MyLongestSubstr mls = new MyLongestSubstr();
		System.out.println(mls.getLongestSubstr("java2novice"));
		System.out.println(mls.getLongestSubstr("java_language_is_sweet"));
		System.out.println(mls.getLongestSubstr("java_java_java_java"));
		System.out.println(mls.getLongestSubstr("abcabcbb"));
	}
}

Output:
[a2novice]
[uage_is]
[_jav, va_j]
[cab, abc, bca]

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

37. Program: Write a program to remove duplicates from sorted array.

Description:
Given array is already sorted, and it has duplicate elements. Write a program to remove duplicate elements and return new array without any duplicate elements. The array should contain only unique elements.


Code:
package com.java2novice.algos;

public class MyDuplicateElements {

	public static int[] removeDuplicates(int[] input){
		
		int j = 0;
        int i = 1;
		//return if the array length is less than 2
        if(input.length < 2){
			return input;
		}
        while(i < input.length){
            if(input[i] == input[j]){
                i++;
            }else{
            	input[++j] = input[i++];
            }    
        }
		int[] output = new int[j+1];
		for(int k=0; k<output.length; k++){
			output[k] = input[k];
		}
		
		return output;
	}
	
	public static void main(String a[]){
		int[] input1 = {2,3,6,6,8,9,10,10,10,12,12};
		int[] output = removeDuplicates(input1);
		for(int i:output){
			System.out.print(i+" ");
		}
	}
}

Output:
2 3 6 8 9 10 12 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

38. Program: How to sort a Stack using a temporary Stack?

Description:
You have a stack with full of integers. Sort it in the ascending order using another temporary array by using all stack functionality.


Code:
package com.java2novice.algo;

import java.util.Stack;

public class StackSort {

	public static Stack<Integer> sortStack(Stack<Integer> input){
		
		Stack<Integer> tmpStack = new Stack<Integer>();
		System.out.println("=============== debug logs ================");
		while(!input.isEmpty()) {
	        int tmp = input.pop();
	        System.out.println("Element taken out: "+tmp);
	        while(!tmpStack.isEmpty() && tmpStack.peek() > tmp) {
	        	input.push(tmpStack.pop());
	        }
	        tmpStack.push(tmp);
	        System.out.println("input: "+input);
	        System.out.println("tmpStack: "+tmpStack);
	    }
		System.out.println("=============== debug logs ended ================");
		return tmpStack;
	}
	
	public static void main(String a[]){
		
		Stack<Integer> input = new Stack<Integer>();
		input.add(34);
		input.add(3);
		input.add(31);
		input.add(98);
		input.add(92);
		input.add(23);
		System.out.println("input: "+input);
		System.out.println("final sorted list: "+sortStack(input));
	}
}

Output:
input: [34, 3, 31, 98, 92, 23]
=============== debug logs ================
Element taken out: 23
input: [34, 3, 31, 98, 92]
tmpStack: [23]
Element taken out: 92
input: [34, 3, 31, 98]
tmpStack: [23, 92]
Element taken out: 98
input: [34, 3, 31]
tmpStack: [23, 92, 98]
Element taken out: 31
input: [34, 3, 98, 92]
tmpStack: [23, 31]
Element taken out: 92
input: [34, 3, 98]
tmpStack: [23, 31, 92]
Element taken out: 98
input: [34, 3]
tmpStack: [23, 31, 92, 98]
Element taken out: 3
input: [34, 98, 92, 31, 23]
tmpStack: [3]
Element taken out: 23
input: [34, 98, 92, 31]
tmpStack: [3, 23]
Element taken out: 31
input: [34, 98, 92]
tmpStack: [3, 23, 31]
Element taken out: 92
input: [34, 98]
tmpStack: [3, 23, 31, 92]
Element taken out: 98
input: [34]
tmpStack: [3, 23, 31, 92, 98]
Element taken out: 34
input: [98, 92]
tmpStack: [3, 23, 31, 34]
Element taken out: 92
input: [98]
tmpStack: [3, 23, 31, 34, 92]
Element taken out: 98
input: []
tmpStack: [3, 23, 31, 34, 92, 98]
=============== debug logs ended ================
final sorted list: [3, 23, 31, 34, 92, 98]

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

39. 

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

40.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

41.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

42.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

43.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
44.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

45.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




